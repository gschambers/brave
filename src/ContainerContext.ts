import React from 'react'
import { container } from 'tsyringe'
import { AuthService } from './services/AuthService'
import { KeyCaptureService } from './services/KeyCaptureService'
import { ProfileService } from './services/ProfileService'

export function bootstrapContainer() {
  container.registerSingleton(AuthService)
  container.registerSingleton(KeyCaptureService)
  container.registerSingleton(ProfileService)

  return container
}

export const ContainerContext = React.createContext(container)
