import { BehaviorSubject } from 'rxjs'
import { map } from 'rxjs/operators'
import { scoped, Lifecycle } from 'tsyringe'
import profile from '../vault/profile.enc.json'
import { IEncryptedPayload, IReadonlyStore } from './types'

export interface ProfileState {
  readonly profile: IEncryptedPayload
}

@scoped(Lifecycle.ContainerScoped)
export class ProfileStore implements IReadonlyStore<ProfileState> {
  private state = new BehaviorSubject({ profile })

  public valueOf<K extends keyof ProfileState>(key: K) {
    return this.state.pipe(map(values => values[key]))
  }
}
