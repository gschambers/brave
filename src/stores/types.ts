import { Observable } from 'rxjs'

export interface IReadonlyStore<T> {
  readonly valueOf: <K extends keyof T>(key: K) => Observable<T[K]>
}

export interface IStore<T> extends IReadonlyStore<T> {
  readonly setValue: <K extends keyof T>(key: K, value: T[K]) => void
}

export interface IEncryptedPayload {
  readonly ciphertext: string
  readonly key: string
  readonly salt: string
  readonly iv: string
}
