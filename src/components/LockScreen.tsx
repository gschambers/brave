import React, { useEffect, useState } from 'react'
import { NEVER, timer } from 'rxjs'
import { catchError, switchMapTo, takeWhile, tap } from 'rxjs/operators'
import { useInstance } from '../hooks/useInstance'
import { AuthService } from '../services/AuthService'
import { KeyCaptureService } from '../services/KeyCaptureService'
import { ProfileService } from '../services/ProfileService'
import './LockScreen.css'

export const LockScreen: React.FC = () => {
  const authService = useInstance(AuthService)
  const keyCaptureService = useInstance(KeyCaptureService)
  const profileService = useInstance(ProfileService)

  const [ pending, setPending ] = useState(true)
  const [ passphrase, setPassphrase ] = useState('')

  useEffect(() => {
    const subscription = keyCaptureService.observeKeySequence()
      .pipe(
        takeWhile(() => pending),
        tap(passphrase => authService.setPassphrase(passphrase))
      )
      .subscribe(setPassphrase)

    return () => subscription.unsubscribe()
  }, [keyCaptureService, pending])

  useEffect(() => {
    const subscription = profileService.observeProfile()
      .pipe(
        catchError(() => NEVER),
        tap(() => setPending(false)),
        switchMapTo(timer(1000)),
      )
      .subscribe(() => authService.unlock())

    return () => subscription.unsubscribe()
  }, [authService, passphrase])

  return (
    <div className={`LockScreen ${pending && 'is-pending'}`}>
      <div className="PasswordForm">
        <div className="PasswordHelpText">
          My profile has been encrypted with a symmetric key. Enter the passphrase to begin.
        </div>

        <div className="PasswordInput">
          {passphrase.split('').map((char, i) => (
            <div key={i} className="PasswordInput-character">{char}</div>
          ))}

          <div className="PasswordInput-cursor" />
        </div>
      </div>

      <div className="LockScreen-logo" />
    </div>
  )
}
