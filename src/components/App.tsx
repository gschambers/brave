import React, { useEffect, useState } from 'react'
import { useInstance } from '../hooks/useInstance'
import { AuthService } from '../services/AuthService'
import { LockScreen } from './LockScreen'
import { Profile } from './Profile'

import './App.css'

export const App: React.FC = () => {
  const authService = useInstance(AuthService)
  const [ locked, setLocked ] = useState(true)

  useEffect(() => {
    const subscription = authService
      .observeLockState()
      .subscribe(setLocked)

    return () => {
      subscription.unsubscribe()
    }
  }, [authService])

  return (
    <div className={`App ${locked && 'is-locked'}`}>
      {locked ? <LockScreen /> : <Profile />}
    </div>
  )
}
